import { Plugin, Setting, PluginSettingTab, App, TAbstractFile, TFile } from "obsidian";

class TagBasedOrganizer extends Plugin {
	async onload() {
		this.addSettingTab(new TagBasedOrganizerSettingsTab(this.app, this));
		this.registerEvent(this.app.vault.on("modify", this.checkAndOrganizeNote.bind(this)));
	}

	async checkAndOrganizeNote(file: TFile) {
		const filePath = file.path;

		const note = await this.app.vault.read(file);
		const tags = this.app.metadataCache.getFileCache(file)?.tags || [];
		const mappings = await this.loadMappings();

		console.log(`Checking ${filePath} for tags ${tags}`);
		for (let mapping of mappings) {
			console.log(`Checking ${filePath} for tag ${mapping.tag}`);
			console.log('has tags: ', tags);

			if (tags.some((tag) => tag.tag === mapping.tag)) {
				var newFilePath = `${mapping.folder}/${file.name}`;
				console.log(`Found tag ${mapping.tag} in ${filePath}. Moving to ${newFilePath}`);

				// if file already exists in the new folder, delete it
				// remove leading slash from newFilePath
				newFilePath = newFilePath.replace(/^\//, '');
				var copyFile = await this.app.vault.getFileByPath(newFilePath);
				console.log("result of search: ", copyFile);
				if (copyFile != null) {
					console.log(`File ${newFilePath} already exists. Deleting`);
					await this.app.vault.delete(copyFile);
				}	

				// copy the file to the new folder
				await this.app.vault.copy(file, newFilePath);
				console.log(`Copied ${filePath} to ${newFilePath}`);
				return;
			}
		}
	}

	async saveMappings(mappings: Mapping[]) {
		await this.saveData(mappings);
	}

	async loadMappings() {
		return await this.loadData();
	}
}

class TagBasedOrganizerSettingsTab extends PluginSettingTab {
	plugin: TagBasedOrganizer;

	constructor(app: App, plugin: TagBasedOrganizer) {
		super(app, plugin);
	}

	async display(): Promise<void> {
		let { containerEl } = this;
		containerEl.empty();
		containerEl.createEl("h2", { text: "Tag-Folder Mappings" });

		// Load existing mappings
		const mappings: Mapping[] = (await this.plugin.loadMappings()) || [];

		// Display existing mappings
		mappings.forEach((mapping) => {
			new Setting(containerEl)
				.setName("Mapping")
				.setDesc("A tag-folder mapping")
				.addText((text) => {
					text.setPlaceholder("Tag")
						.setValue(mapping.tag)
						.onChange(async (value) => {
							mapping.tag = value;
							await this.plugin.saveMappings(mappings);
						});
				})
				.addText((text) => {
					text.setPlaceholder("Folder")
						.setValue(mapping.folder)
						.onChange(async (value) => {
							mapping.folder = value;
							await this.plugin.saveMappings(mappings);
						});
				})
				.addExtraButton((button) => {
					button
						.setIcon("cross")
						.setTooltip("Remove this mapping")
						.onClick(async () => {
							mappings.splice(mappings.indexOf(mapping), 1);
							await this.plugin.saveMappings(mappings);
							this.display(); // Refresh the display
						});
				});
		});

		// button to add new mapping
		new Setting(containerEl)
			.setName("Add Mapping")
			.setDesc("Add a new tag-folder mapping")
			.addButton((button) => {
				button.setButtonText("Add");
				button.onClick(async () => {
					mappings.push(new Mapping("", ""));
					await this.plugin.saveMappings(mappings);
					this.display(); // Refresh the display
				});
			});
	}
}

class Mapping {
	tag: string;
	folder: string;

	constructor(tag: string, folder: string) {
		this.tag = tag;
		this.folder = folder;
	}
}

module.exports = TagBasedOrganizer;
